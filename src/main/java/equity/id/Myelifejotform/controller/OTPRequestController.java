package equity.id.Myelifejotform.controller;

import equity.id.Myelifejotform.dto.OTPRequest.OTPRequestDto;
import equity.id.Myelifejotform.model.OTPRequest;
import equity.id.Myelifejotform.retrofit.otp.OTPNotificationResponse;
import equity.id.Myelifejotform.retrofit.otp.OTPNotificationService;
import equity.id.Myelifejotform.service.OTPRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import java.io.IOException;
import java.util.HashMap;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/")
public class OTPRequestController {

    @Autowired
    private OTPNotificationService otpNotificationService;

    @Autowired
    private OTPRequestService otpRequestService;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("generate-otp")
    public ResponseEntity listOTP(@RequestBody OTPRequest otpRequest, String getOTP) throws IOException {
        String otpData = otpRequestService.generateOTP(otpRequest, getOTP);
        OTPNotificationResponse data = otpNotificationService.postOTPNotification(otpRequest, otpData);
        return ResponseEntity.ok(data);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("verify-otp")
    public ResponseEntity listVerifyOtp(@RequestParam(name = "otp") @Max(6) String otp){
        OTPRequestDto otpData = otpRequestService.checkOtp(otp);
        HashMap<String, String> data = new HashMap<>();
        if (otpData != null){
            return ResponseEntity.ok(null);
        }
        data.put("otp", otp);
        return ResponseEntity.ok(data);
    }
}
