package equity.id.Myelifejotform.retrofit.edits;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EditResponse<T> {
    private String error_code;
    private String error_description;
    private T data;

    public EditResponse(String error_code, String error_description, T data) {
        this.error_code = error_code;
        this.error_description = error_description;
        this.data = data;
    }
}
