package equity.id.Myelifejotform.retrofit.otp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface OTPNotificationApi {

    @POST("notification")
    Call<OTPNotificationResponse> sendOTP(@Body OTPNotificationRequest request);
}
