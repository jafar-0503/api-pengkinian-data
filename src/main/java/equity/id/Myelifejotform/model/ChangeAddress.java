package equity.id.Myelifejotform.model;

import equity.id.Myelifejotform.auditable.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_change_address")
public class ChangeAddress extends Auditable<String> implements Serializable {

    @Column(length = 10, name = "change_address_code")
    private String changeAddressCode;

    @Column(length = 12, name = "policy_no")
    private String policyNo;

    @Column(length = 100, name = "policy_holder")
    private String policyHolder;

    @Column(length = 30, name = "handphone_no")
    private String handphoneNo;

    @Column(length = 60, name = "email_address")
    private String emailAddress;

    @Column(name = "is_change_data")
    private boolean isChangeData;

    @Column(length = 30, name = "new_handphone_no")
    private String newHandphoneNo;

    @Column(name = "is_whatsapp_no")
    private boolean isWhatsappNumber;

    @Column(length = 30, name = "whatsapp_no")
    private String whatsappNo;

    @Column(length = 60, name = "new_email_address")
    private String newEmailAddress;

    @Column(name = "is_change_address")
    private boolean isChangeAddress;

    @Column(length = 120, name = "address_1")
    private String address1;

    @Column(length = 120, name = "address_2")
    private String address2;

    @Column(length = 120, name = "address_3")
    private String address3;

    @Column(length = 60, name = "city_code")
    private String cityCode;

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "state_id", referencedColumnName = "id")
    @Column(length = 4, name = "state_code")
    private String stateCode;

    @Column(length = 5, name = "postal_code")
    private String postalCode;

    @Column(length = 4, name = "country_code")
    private String countryCode;

    @Column(length = 30, name = "fax_no")
    private String faxNo;

    @Column(length = 30, name = "phone_no")
    private String phoneNo;

    @NotEmpty(message = "Image may not be empty")
    @Column(name = "image")
    private String image;

    @Column(length = 30, name = "transaction_code")
    private String transactionCode;

    @Column(length = 15, name = "transaction_status")
    private String transactionStatus;

    @Column(name = "is_active")
    private boolean isActive;
}
