package equity.id.Myelifejotform.service;

import equity.id.Myelifejotform.config.error.NotFoundException;
import equity.id.Myelifejotform.dto.State.StateDto;
import equity.id.Myelifejotform.dto.State.CreateStateDto;
import equity.id.Myelifejotform.model.State;
import equity.id.Myelifejotform.repository.StateRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class StateService {

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data State
    public ResponseEntity<List<State>> listState(){
        List<State> states = stateRepository.findAll();
        Type targetType = new TypeToken<List<State>>(){}.getType();
        List<State> response= modelMapper.map(states, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data Base on ID
    public ResponseEntity<State> getStateById(Long id){
        State state = stateRepository.findById(id).orElseThrow(()-> new NotFoundException("Data State ID " + id + " is not exist"));
        State response = modelMapper.map(state, State.class);

        return ResponseEntity.ok(response);
    }

    //Post Data State
    public ResponseEntity<StateDto> addState(StateDto newState){
        State state = modelMapper.map(newState, State.class);
        State stateSaved = stateRepository.save(state);
        StateDto response = modelMapper.map(stateSaved, StateDto.class);

        return ResponseEntity.ok(response);
    }

    //Update Data State
    public ResponseEntity<State> editState(State updateState, Long id){
        State state = stateRepository.findById(id).orElseThrow(()-> new NotFoundException("Data State ID " + id + " is not exist"));
        state.setStateCode(updateState.getStateCode());
        state.setDescription(updateState.getDescription());
        state.setActive(updateState.isActive());
        stateRepository.save(state);

        State response = modelMapper.map(state, State.class);
        return ResponseEntity.ok(response);
    }

    //Remove Data State
    public ResponseEntity<State> deleteState(Long id){
        State state = stateRepository.findById(id).orElseThrow(()-> new NotFoundException("Data State ID " + id + " is not exist"));
        stateRepository.deleteById(id);
        State response = modelMapper.map(state, State.class);

        return ResponseEntity.ok(response);
    }
}
